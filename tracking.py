#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Libraries:

import os
import numpy as np
import math as m
from glob import glob
import cv2
from scipy import ndimage as nd
from scipy.optimize import minimize
from matplotlib.widgets import RectangleSelector
from matplotlib import pyplot as plt
from matplotlib.widgets import Button

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
#Inputs:

## Folder where to look for images:
nam_fold = 'image_test'

## Picture extensions:
pict_ext = '.png'


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Define useful functions:

## Fonctions to select the pattern positions:
def line_select_callback(eclick,erelease):
    global x1, y1, x2, y2
    x1,y1=eclick.xdata,eclick.ydata
    x2,y2=erelease.xdata,erelease.ydata

def toggle_selector(event):
    print(' Key pressed.')
    if event.key in ['Q', 'q'] and toggle_selector.RS.active:
        print(' RectangleSelector deactivated.')
        toggle_selector.RS.set_active(False)
    if event.key in ['A', 'a'] and not toggle_selector.RS.active:
        print(' RectangleSelector activated.')
        toggle_selector.RS.set_active(True)

def select(self):
    global current_ax, x1, y1, x2, y2, list_area
    current_ax.plot([x1,x2, x2, x1, x1],[y1, y1, y2, y2, y1],'-r')
    list_area.append(np.array([x1, y1, x2, y2]).astype('int'))

## Define correlation fonction for tracking:
def CorrelVal(X,roi_0,roi_1):
    rr=0.1
    ## Extraction of the variables:
    dI=X[0]
    dJ=X[1]
    
    if max(X)>3:
        s=1e10
    else:
        ## Shift the picture:
        roi_1_rs=nd.interpolation.shift(roi_1,(dI,dJ),order=1,mode='constant',cval=0.,prefilter=False)
        ## Reduction to the center of the ROI:
        roi_0_r=roi_0[int(len(roi_0)*rr):int(len(roi_0)*(1.-rr)),int(len(roi_0)*rr):int(len(roi_0)*(1.-rr))]
        roi_1_rs_r=roi_1_rs[int(len(roi_0)*rr):int(len(roi_0)*(1.-rr)),int(len(roi_0)*rr):int(len(roi_0)*(1.-rr))]
        ## Correlation:
        s=np.sum(abs(roi_1_rs_r-roi_0_r))/(len(roi_0)**2)
        # ~ s=1/(np.sum(roi_1_rs_r*roi_0_r)/(len(roi_0)**2))
    
    return s

## Main fonctions to detect pattern motion:
def MainTrack(it_a):
    ### Define global variables:
    global list_area, image_0, image_1, IJ_stp, it_img
    
    ### Load area coordinates:
    x1=list_area[it_a][0]
    x2=list_area[it_a][2]
    y1=list_area[it_a][1]
    y2=list_area[it_a][3]
    
    ### Load current position :
    I_cur=int(IJ_stp[it_a,0,it_img])
    J_cur=int(IJ_stp[it_a,1,it_img])
    I_n=int(IJ_stp[it_a,0,it_img]) 
    J_n=int(IJ_stp[it_a,1,it_img])
    
    ### FFT approximation:
    #### Extraction of the ROI:
    I_roi_m_0=y1; I_roi_M_0=y2; J_roi_m_0=x1; J_roi_M_0=x2
    roi_0=image_0[I_roi_m_0:I_roi_M_0,J_roi_m_0:J_roi_M_0]
    
    # ~ from PIL import Image
    # ~ roi_plot=roi_0.astype('uint8')
    # ~ img=Image.fromarray(roi_plot)
    # ~ img.save('0.png')
    
    I_roi_m_1=y1+I_n; I_roi_M_1=I_roi_m_1+roi_0.shape[0]; J_roi_m_1=x1+J_n; J_roi_M_1=J_roi_m_1+roi_0.shape[1]
    roi_1=image_1[I_roi_m_1:I_roi_M_1,J_roi_m_1:J_roi_M_1]
    
    # ~ roi_plot=roi_1.astype('uint8')
    # ~ img=Image.fromarray(roi_plot)
    # ~ img.save('1.png')
    
    #### Measurement shift between picture from FFT:
    ##### Compute Fourier transform:
    cell0FFT=np.fft.fft2(roi_0)
    cell1FFT=np.conjugate(np.fft.fft2(roi_1))
    ##### Convolute:
    pictCCor=np.real(np.fft.ifft2((cell0FFT*cell1FFT)))
    ##### Compute the shift: 
    pictCCorShift=np.fft.fftshift(pictCCor)
    I_sft,J_sft=np.where(pictCCorShift==np.max(pictCCorShift))
    I_sft=I_sft[0]-int(roi_0.shape[0]/2)
    J_sft=J_sft[0]-int(roi_0.shape[1]/2)
    #### Set the new approximate position:
    I_n=I_n-I_sft
    J_n=J_n-J_sft
    
    ### Extract the new ROI:
    I_roi_m_n=y1+I_n; I_roi_M_n=I_roi_m_n+roi_0.shape[0]; J_roi_m_n=x1+J_n; J_roi_M_n=J_roi_m_n+roi_0.shape[1]
    roi_1=image_1[I_roi_m_n:I_roi_M_n,J_roi_m_n:J_roi_M_n]
    
    # ~ roi_plot=roi_1.astype('uint8')
    # ~ img=Image.fromarray(roi_plot)
    # ~ img.save('2.png')
    
    ### Optimisation of the correlation:
    ext_res=minimize(CorrelVal,np.array([0.,0.]),args=(roi_0,roi_1),method='Nelder-Mead',tol=1e-10,options={'maxiter':3000,'maxfev':5000,'disp':False}) 
    ### Final and accurate position measurement: 
    dI_0=ext_res.x[0]
    dJ_0=ext_res.x[1]
    ###Store new position:
    dI_0=I_n-dI_0
    dJ_0=J_n-dJ_0
    corr_0=ext_res.fun
    IJ_stp[it_a,0,it_img+1]=dI_0
    IJ_stp[it_a,1,it_img+1]=dJ_0
    IJ_stp[it_a,2,it_img+1]=corr_0

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Select areas to track:

## Get and sort current picture names:
nam_pict = sorted(glob(nam_fold+'/*'+pict_ext))

## Load the first image:
image_0 = cv2.imread(nam_pict[0])

## Select manually:
list_area=[]
fig,current_ax = plt.subplots()
plt.imshow(image_0)
plt.title('drag a rectangles and select, then close')
plt.axis('off')
ax_select = plt.axes([0.42, 0.03, 0.2, 0.05])
b_select = Button(ax_select, 'Select')
b_select.on_clicked(select)
toggle_selector.RS = RectangleSelector(current_ax,line_select_callback,drawtype='box',useblit=True,button=[1,3],minspanx=5,minspany=5,spancoords='pixels',interactive=True)
plt.connect('key_press_event',toggle_selector)
plt.show()
plt.close()

## Plot area for record
os.system('mkdir figure')
plt.imshow(image_0)
for it_c in range(len(list_area)):
    x1=list_area[it_c][0]
    x2=list_area[it_c][2]
    y1=list_area[it_c][1]
    y2=list_area[it_c][3]
    plt.plot([x1,x2, x2, x1, x1],[y1, y1, y2, y2, y1],'-r')
    plt.text(0.5*(x1+x2),0.5*(y1+y2),str(it_c),horizontalalignment='center',verticalalignment='center',color='r',fontsize=15)

plt.axis('off')
plt.savefig('figure/selected_area.png')
plt.title('close')
plt.show()
plt.close()

## Save areas:
os.system('mkdir result')
np.savetxt('result/area.txt',np.vstack(list_area))

#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# Track areas:

## Get the number of pictures:
nb_pict=len(nam_pict)

## Get the number of areas:
nb_area=len(list_area)

## Initialize storage:
IJ_0=np.zeros((nb_area,2))
IJ_stp=np.zeros((nb_area,3,nb_pict))
IJ_stp[:,0:2,0]=IJ_0

## Load the initial picture:
image_0=cv2.imread(nam_pict[0],0).astype('float')

## Loop over the pictures:
for it_img in range(nb_pict-1):
    print('picture '+str(it_img))
    ### Load the current picture:
    image_1=cv2.imread(nam_pict[it_img+1],0).astype('float')
    ### Loop over the areas:
    for it_a in range(nb_area):
        MainTrack(it_a)

## Save pattern positions:
np.savetxt('result/I_step.txt',IJ_stp[:,0,:],fmt='%f')
np.savetxt('result/J_step.txt',IJ_stp[:,1,:],fmt='%f')
np.savetxt('result/correlation_step.txt',IJ_stp[:,2,:],fmt='%f')


#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#-|-#
# plot results:

os.system('mkdir tmp')

## Loop over images:
for it_img in range(nb_pict-1):
    print('picture '+str(it_img))
    ### Load the current picture:
    image_1=cv2.imread(nam_pict[it_img],0).astype('float')
    ### Load current positions:
    I_c=IJ_stp[:,0,it_img]
    J_c=IJ_stp[:,1,it_img]
    ### Display:
    plt.imshow(image_1)
    plt.axis('off')
    plt.title(str(it_img))
    for it_c in range(len(list_area)):
        x1=list_area[it_c][0]
        x2=list_area[it_c][2]
        y1=list_area[it_c][1]
        y2=list_area[it_c][3]
        plt.plot([x1,x2, x2, x1, x1]+J_c[it_c],[y1, y1, y2, y2, y1]+I_c[it_c],'-r',linewidth=0.5)
    
    plt.savefig('tmp/'+str(it_img).zfill(5)+'.png',dpi=150)
    plt.close()

os.system('ffmpeg -y -r  10 -f image2 -i tmp/%05d.png -qscale 10 figure/movie.avi')
os.system('rm -rf tmp')



